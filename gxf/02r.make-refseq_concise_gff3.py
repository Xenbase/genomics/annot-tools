#!/usr/bin/env python3
import sys

filename_gff3 = sys.argv[1]

f_gff3 = open(filename_gff3, 'r')
if filename_gff3.endswith('.gz'):
    import gzip
    f_gff3 = gzip.open(filename_gff3, 'rt')

filename_base = filename_gff3.replace('.gff3', '')
f_coding = open('%s_coding.gff3' % filename_base, 'w')
f_coding.write("##gff-version 3\n")
f_noncoding = open('%s_noncoding.gff3' % filename_base, 'w')
f_noncoding.write("##gff-version 3\n")

tx2gene = dict()
is_coding = dict()

gene2type = dict()
gene2name = dict()

for line in f_gff3:
    if line.startswith('#'):
        print(line.strip())
        continue

    tokens = line.strip().split("\t")

    attr_list = dict()
    for tmp_attr in tokens[8].split(';'):
        tmp_attr = tmp_attr.strip()
        for tmp_key in ['ID', 'Name', 'gene_biotype',
                        'Parent', 'transcript_id', 'protein_id']:
            if tmp_attr.startswith(tmp_key):
                tmp_attr = tmp_attr.replace('%s=' % tmp_key, '')
                attr_list[tmp_key] = tmp_attr.replace('"', '')
        if tmp_attr.startswith('Dbxref='):
            for tmp2 in tmp_attr.split('=')[1].split(','):
                if tmp2.startswith('GeneID:'):
                    attr_list['GeneID'] = tmp2

    tmp_feature = tokens[2]
    tmp_new_attr = ''

    if tmp_feature == 'gene':
        tmp_ID = attr_list['GeneID']
        tmp_biotype = attr_list['gene_biotype']
        tmp_gene_id = attr_list['GeneID']
        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)

        gene2type[tmp_ID] = tmp_biotype
        if tmp_biotype == 'protein_coding':
            is_coding[tmp_ID] = 1
            is_coding[tmp_gene_id] = 1

    elif tmp_feature == 'pseudogene':
        tmp_ID = attr_list['GeneID']
        tmp_biotype = attr_list['gene_biotype']
        tmp_gene_id = attr_list['GeneID']
        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)
        gene2type[tmp_ID] = tmp_biotype

    elif tmp_feature == 'mRNA' or tmp_feature == 'lnc_RNA' \
            or tmp_feature == 'transcript':
        tmp_ID = attr_list['transcript_id']
        tmp_parent = attr_list['GeneID']
        tmp_biotype = gene2type[tmp_parent]
        tmp_gene_id = tmp_parent
        tmp_tx_id = attr_list['transcript_id']
        tmp_new_attr = \
            'ID=%s;Parent=%s;biotype=%s;transcript_id=%s;gene_id=%s;' %\
            (tmp_ID, tmp_parent, tmp_biotype, tmp_tx_id, tmp_gene_id)
        tx2gene[tmp_ID] = tmp_gene_id

        # "transcript" seems to be a feature derived from RNA-seq,
        # but they are mostly a part of mRNA.
        # for consistency, I changed them to "mRNA" for protein_coding types.
        if tmp_feature == 'transcript' and tmp_biotype == 'protein_coding':
            tokens[2] = 'mRNA'
            tmp_feature = 'mRNA'

    elif tmp_feature == 'exon':
        tmp_ID = attr_list['ID']
        tmp_parent = attr_list['Parent'].replace('rna-', '')
        tmp_tx_id = tmp_parent
        tmp_gene_id = 'NA'
        if tmp_tx_id in tx2gene:
            tmp_gene_id = tx2gene[tmp_tx_id]

        tmp_new_attr = 'ID=%s;Parent=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_tx_id, tmp_gene_id)

    elif tmp_feature == 'CDS':
        tmp_ID = attr_list['ID']
        tmp_parent = attr_list['Parent'].replace('rna-', '')
        tmp_tx_id = tmp_parent
        tmp_gene_id = 'NA'
        if tmp_tx_id in tx2gene:
            tmp_gene_id = tx2gene[tmp_tx_id]

        tmp_new_attr = 'ID=%s;Parent=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_tx_id, tmp_gene_id)

        tmp_prot_id = 'NA'
        if 'protein_id' in attr_list:
            tmp_prot_id = attr_list['protein_id']
        tmp_new_attr += 'protein_id=%s;' % (tmp_prot_id)

    else:
        tmp_ID = attr_list['ID']
        tmp_parent = 'NA'
        if 'GeneID' in attr_list:
            tmp_parent = attr_list['GeneID']
        elif 'Parent' in attr_list:
            tmp_parent = attr_list['Parent']
        # else:
        #    print("No parent:", line.strip())
        #    sys.exit(1)

        tmp_biotype = 'NA'
        tmp_gene_id = 'NA'
        if 'biotype' in attr_list:
            tmp_biotype = attr_list['biotype']
        elif tmp_parent in tx2gene:
            tmp_gene_id = tx2gene[tmp_parent]
            tmp_biotype = gene2type[tmp_gene_id]

        tmp_tx_id = 'NA'
        if tmp_parent in tx2gene:
            tmp_tx_id = tmp_parent

        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)
        if tmp_tx_id != 'NA':
            tmp_new_attr += 'Parent=%s;transcript_id=%s;' %\
                            (tmp_parent, tmp_tx_id)

    if tmp_feature == 'gene':
        if 'Name' in attr_list:
            gene2name[tmp_gene_id] = attr_list['Name']

    if tmp_feature == 'gene' or tmp_feature == 'mRNA':
        tmp_name = 'NA'
        if tmp_gene_id in gene2name:
            tmp_name = gene2name[tmp_gene_id]

        if tmp_new_attr != '' and tmp_name != 'NA':
            tmp_new_attr += 'Name=%s;' % (tmp_name)

    str_coord = "\t".join(tokens[:8])

    if tmp_new_attr != '' and tmp_gene_id in is_coding:
        f_coding.write("%s\t%s\n" % (str_coord, tmp_new_attr))

    else:
        f_noncoding.write("%s\t%s\n" % (str_coord, tmp_new_attr))
        # f_noncoding.write("%s\n" % line.strip())

f_gff3.close()

f_coding.close()
f_noncoding.close()
