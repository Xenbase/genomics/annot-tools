#!/usr/bin/env python3
import re
import sys
import gzip

filename_in = sys.argv[1]
filename_base = re.sub('.gff3[.gz]*', '', filename_in)

f_ensembl = open("%s_ensembl.gff3" % filename_base, "w")
f_RefSeq = open("%s_RefSeq.gff3" % filename_base, "w")
f_others = open("%s_others.gff3" % filename_base, "w")

f_in = open(filename_in, 'r')
if filename_in.endswith('.gz'):
    f_in = gzip.open(filename_in, 'rt')

for line in f_in:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_source = tokens[1]

    if tmp_source.endswith("ensembl"):
        f_ensembl.write("%s\n" % line.strip())
    elif tmp_source == "RefSeq":
        f_RefSeq.write("%s\n" % line.strip())
    else:
        f_others.write("%s\n" % line.strip())
f_in.close()

f_others.close()
f_RefSeq.close()
f_ensembl.close()
