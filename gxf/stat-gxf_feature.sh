#!/bin/bash

cat $1 | grep -v ^# | awk -F"\t" '{print $3}' | sort | uniq -c | sort -n
