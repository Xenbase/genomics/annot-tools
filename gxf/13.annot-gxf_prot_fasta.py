#!/usr/bin/env python3

import sys

filename_prot_fa = sys.argv[1]
filename_gtf = filename_prot_fa.replace('_prot.fa', '')

prot_info = dict()
gene2prot = dict()
gene_names = dict()

f_gtf = open(filename_gtf, 'r')
for line in f_gtf:
    if line.startswith('#'):
        continue

    tokens = line.strip().split("\t")
    tmp_mol_type = tokens[2]

    attr_list = dict()
    for tmp in tokens[8].strip().split(';'):
        if tmp.strip() == '':
            continue
        (tmp_k, tmp_v) = tmp.strip().split("=")
        # tmp_tokens = tmp.split()
        # tmp_k = tmp_tokens[0]
        # tmp_v = tmp_tokens[1]
        tmp_k = tmp_k.strip()
        tmp_v = tmp_v.replace('"', '')
        attr_list[tmp_k] = tmp_v

    if tmp_mol_type == 'gene':
        gene_name = 'NA'
        if 'Name' in attr_list:
            gene_id = attr_list['gene_id']
            gene_name = attr_list['Name']
            gene_names[gene_id] = gene_name
    
    if tmp_mol_type == 'CDS':
        prot_id = attr_list['protein_id']
        tx_id = attr_list['transcript_id']
        gene_id = attr_list['gene_id']
        gene_name = 'NA'
        if gene_id in gene_names:
            gene_name = gene_names[gene_id]


        prot_info[tx_id] = '%s|%s transcript_id=%s gene_id=%s' % (gene_name, prot_id, tx_id, gene_id)
            
        if gene_id not in gene2prot:
            gene2prot[gene_id] = dict()
        gene2prot[gene_id][tx_id] = 1
f_gtf.close()

filename_out_base = filename_prot_fa.replace('.fa', '')
filename_out_all = '%s_annot_all.fa' % filename_out_base
filename_out_longest = '%s_annot_longest.fa' % filename_out_base

prot_len = dict()
prot_seq = dict()

f_out_all = open(filename_out_all, 'w')
f_fa = open(filename_prot_fa, 'r')
for line in f_fa:
    if line.startswith('>'):
        tx_id = line.strip().lstrip('>').split()[0]
        # cds_info = "CDS=NA"
        # if line.find('CDS=') >= 0:
        #     cds_info = line.strip().lstrip('>').split()[1]
        prot_len[tx_id] = 0
        prot_seq[tx_id] = []
        # f_out_all.write('>%s %s\n' % (prot_info[tx_id], cds_info))
        f_out_all.write('>%s\n' % (prot_info[tx_id]))
    else:
        prot_len[tx_id] += len(line.strip())
        prot_seq[tx_id].append(line.strip())
        f_out_all.write("%s\n" % line.strip())
f_fa.close()
f_out_all.close()

f_out_longest = open(filename_out_longest, 'w')
for tmp_g in gene2prot.keys():
    tmp_prot_list = sorted(gene2prot[tmp_g].keys(), key=prot_len.get)
    tmp_t_longest = tmp_prot_list[-1]
    f_out_longest.write(">%s\n" % prot_info[tmp_t_longest])
    f_out_longest.write("%s\n" % "\n".join(prot_seq[tmp_t_longest]))
f_out_longest.close()
