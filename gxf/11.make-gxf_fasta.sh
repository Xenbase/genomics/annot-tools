#!/bin/bash

GXF=$1
GENOME=$2

TX_FA=$GXF"_tx.fa"
TX_LOG=$GXF"_tx.log"

gffread -v -w $TX_FA -g $GENOME $GXF &>$TX_LOG
echo $(grep '>' $TX_FA | wc -l) "sequences on "$TX_FA

CDS_FA=$GXF"_cds.fa"
CDS_LOG=$GXF"_cds.log"
gffread -v -x $CDS_FA -g $GENOME $GXF &>$CDS_LOG
echo $(grep '>' $CDS_FA | wc -l) "sequences on "$CDS_FA

PROT_FA=$GXF"_prot.fa"
PROT_LOG=$GXF"_prot.log"
gffread -v -y $PROT_FA -g $GENOME $GXF &>$PROT_LOG
echo $(grep '>' $PROT_FA | wc -l) "sequences on "$PROT_FA

