#!/bin/bash

GXF=$1
cat $GXF | grep -v ^# | awk -F"\t" '{print $2"\t"$3}' | sort | uniq -c | sort -n
