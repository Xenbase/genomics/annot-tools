#!/usr/bin/env python3
import re
import sys
import gzip

# expecting *.raw.gff3.gz or *.raw.gff3
filename_in = sys.argv[1]
filename_base = re.sub('.gff3[.gz]*', '', filename_in)

f_Gnomon = open("%s_Gnomon.gff3" % filename_base, "w")
f_cmsearch = open("%s_cmsearch.gff3" % filename_base, "w")
f_tRNAscan = open("%s_tRNAscan.gff3" % filename_base, "w")

f_RefSeq = open("%s_RefSeq.gff3" % filename_base, "w")
f_BestRefSeq = open("%s_BestRefSeq.gff3" % filename_base, "w")
f_others = open("%s_others.gff3" % filename_base, "w")


f_in = open(filename_in, 'r')
if filename_in.endswith('.gz'):
    f_in = gzip.open(filename_in, 'rt')

for line in f_in:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_source = tokens[1]

    if tmp_source.endswith("Gnomon"):
        f_Gnomon.write("%s\n" % line.strip())
        if tmp_source.find('BestRefSeq') >= 0:
            f_BestRefSeq.write("%s\n" % line.strip())
    elif tmp_source == "cmsearch":
        f_cmsearch.write("%s\n" % line.strip())
    elif tmp_source == "tRNAscan-SE":
        f_tRNAscan.write("%s\n" % line.strip())
    elif tmp_source == "BestRefSeq":
        f_BestRefSeq.write("%s\n" % line.strip())
    elif tmp_source == "RefSeq":
        f_RefSeq.write("%s\n" % line.strip())
    else:
        f_others.write("%s\n" % line.strip())
f_in.close()

f_others.close()
f_RefSeq.close()
f_BestRefSeq.close()
f_tRNAscan.close()
f_cmsearch.close()
f_Gnomon.close()
