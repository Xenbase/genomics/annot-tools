#!/bin/bash

GXF=$1
SCRIPT_NAME=$(basename "$0")
USAGE="  Usage: $SCRIPT_NAME <GTF/GFF3>"

if [[ $# -eq 0 ]] ; then
  echo $USAGE
  exit 1
fi

if [ -f $GXF ]; then
  SORTED=$GXF".sorted.gz"
  (grep "^#" $GXF | sort -u; grep -v "^#" $GXF | sort -u | sort -t"`printf '\t'`" -k1,1 -k4,4n) | bgzip > $SORTED

  # then, run tabix
  # $ tabix <GTF/GFF3 file>

  # tabix $SORTED
else
  echo $USAGE
  exit
fi
