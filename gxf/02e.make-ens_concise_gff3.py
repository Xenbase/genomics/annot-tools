#!/usr/bin/env python3
import sys

filename_gff3 = sys.argv[1]

f_gff3 = open(filename_gff3, 'r')
if filename_gff3.endswith('.gz'):
    import gzip
    f_gff3 = gzip.open(filename_gff3, 'rt')

filename_base = filename_gff3.replace('.gff3', '')
f_coding = open('%s_coding.gff3' % filename_base, 'w')
f_coding.write("##gff-version 3\n")
f_noncoding = open('%s_noncoding.gff3' % filename_base, 'w')
f_noncoding.write("##gff-version 3\n")

tx2gene = dict()
is_coding = dict()

for line in f_gff3:
    if line.startswith('#'):
        print(line.strip())
        continue

    tokens = line.strip().split("\t")
    str_coord = "\t".join(tokens[:8])

    attr_list = dict()
    for tmp_attr in tokens[8].split(';'):
        tmp_attr = tmp_attr.strip()
        for tmp_key in ['ID', 'Name', 'biotype', 'gene_id', 'version',
                        'Parent', 'transcript_id', 'exon_id', 'protein_id']:
            if tmp_attr.startswith(tmp_key):
                tmp_attr = tmp_attr.replace('%s=' % tmp_key, '')
                attr_list[tmp_key] = tmp_attr.replace('"', '')

    tmp_feature = tokens[2]
    tmp_new_attr = ''

    if tmp_feature == 'gene':
        tmp_ID = attr_list['ID'].split(':')[1]
        tmp_biotype = attr_list['biotype']
        tmp_gene_id = attr_list['gene_id']
        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)
        # tmp_new_attr += 'gene_version=%s;' % (attr_list['version'])

        if tmp_biotype == 'protein_coding':
            is_coding[tmp_ID] = 1
            is_coding[tmp_gene_id] = 1

    elif tmp_feature == 'ncRNA_gene':
        tmp_ID = attr_list['ID'].split(':')[1]
        tmp_biotype = attr_list['biotype']
        tmp_gene_id = attr_list['gene_id']
        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)
        # tmp_new_attr += 'gene_version=%s;' % (attr_list['version'])

    elif tmp_feature == 'pseudogene':
        tmp_ID = attr_list['ID'].split(':')[1]
        tmp_biotype = attr_list['biotype']
        tmp_gene_id = attr_list['gene_id']
        tmp_new_attr = 'ID=%s;biotype=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_biotype, tmp_gene_id)
        # tmp_new_attr += 'gene_version=%s;' % (attr_list['version'])

    elif tmp_feature == 'mRNA':
        tmp_ID = attr_list['ID'].split(':')[1]
        tmp_biotype = attr_list['biotype']
        tmp_parent = attr_list['Parent'].split(':')[1]
        tmp_gene_id = tmp_parent.replace('gene:', '')
        tmp_tx_id = attr_list['transcript_id']
        tmp_new_attr = 'ID=%s;Parent=%s;biotype=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_biotype, tmp_tx_id, tmp_gene_id)
        # tmp_new_attr += 'transcript_version=%s;' % (attr_list['version'])
        tx2gene[tmp_ID] = tmp_gene_id
        tx2gene[tmp_tx_id] = tmp_gene_id

    elif tmp_feature == 'exon':
        tmp_ID = attr_list['exon_id']
        tmp_parent = attr_list['Parent'].split(':')[1]
        tmp_tx_id = tmp_parent.replace('transcript:', '')
        tmp_gene_id = 'NA'
        if tmp_tx_id in tx2gene:
            tmp_gene_id = tx2gene[tmp_tx_id]

        tmp_new_attr = 'ID=%s;Parent=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_tx_id, tmp_gene_id)
        # tmp_new_attr += 'exon_version=%s;' % (attr_list['version'])

    elif tmp_feature == 'five_prime_UTR' or tmp_feature == 'three_prime_UTR':
        tmp_parent = attr_list['Parent'].split(':')[1]
        tmp_tx_id = tmp_parent.replace('transcript:', '')
        tmp_gene_id = 'NA'
        if tmp_tx_id in tx2gene:
            tmp_gene_id = tx2gene[tmp_tx_id]

        tmp_new_attr = 'Parent=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_parent, tmp_tx_id, tmp_gene_id)

    elif tmp_feature == 'CDS':
        tmp_ID = attr_list['ID']
        tmp_parent = attr_list['Parent'].split(':')[1]
        tmp_tx_id = tmp_parent.replace('transcript:', '')
        tmp_gene_id = 'NA'
        if tmp_tx_id in tx2gene:
            tmp_gene_id = tx2gene[tmp_tx_id]

        tmp_new_attr = 'ID=%s;Parent=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_tx_id, tmp_gene_id)

        tmp_new_attr += 'protein_id=%s;' % (attr_list['protein_id'])
    else:
        tmp_ID = attr_list['ID']
        tmp_biotype = attr_list['biotype']
        tmp_parent = 'NA'
        if 'Parent' in attr_list:
            tmp_parent = attr_list['Parent'].split(':')[1]
        else:
            print("No parent:", line.strip())
            sys.exit(1)

        tmp_gene_id = tmp_parent.replace('gene:', '')
        tmp_tx_id = attr_list['transcript_id']
        tmp_new_attr = 'ID=%s;Parent=%s;biotype=%s;transcript_id=%s;gene_id=%s;' %\
                       (tmp_ID, tmp_parent, tmp_biotype, tmp_tx_id, tmp_gene_id)
        # tmp_new_attr += 'transcript_version=%s;' % (attr_list['version'])
        tx2gene[tmp_ID] = tmp_gene_id
        tx2gene[tmp_tx_id] = tmp_gene_id

    if tmp_feature == 'gene' or tmp_feature == 'mRNA':
        if tmp_new_attr != '' and 'Name' in attr_list:
            tmp_new_attr += 'Name=%s;' % (attr_list['Name'])

    if tmp_new_attr != '' and tmp_gene_id in is_coding:
        f_coding.write("%s\t%s\n" % (str_coord, tmp_new_attr))

    else:
        f_noncoding.write("%s\t%s\n" % (str_coord, tmp_new_attr))
        # f_noncoding.write("%s\n" % line.strip())

f_gff3.close()

f_coding.close()
f_noncoding.close()
