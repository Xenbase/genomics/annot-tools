#!/usr/bin/env python3
import os
import sys
import re

filename_gxf = sys.argv[1]
filename_base = os.path.basename(filename_gxf)

f_gxf = open(filename_gxf, 'r')
if filename_gxf.endswith('.gz'):
    import gzip
    f_gxf = gzip.open(filename_gxf, 'rt')
    filename_base = re.sub(r'.gz$', '', filename_base)

file_type = 'gxf'
if filename_base.endswith('.gff3'):
    file_type = 'gff3'
    filename_base = re.sub(r'.gff3$', '', filename_base)
if filename_base.endswith('.gtf'):
    file_type = 'gtf'
    filename_base = re.sub(r'.gtf$', '', filename_base)

version_str = 'NA'
gxf_list = {'chrUn': []}
for line in f_gxf:
    if line.startswith('#'):
        if line.find('version'):
            version_str = line.strip()
        continue
    
    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    if seq_id not in gxf_list:
        if not seq_id.startswith('chrUn'):
            gxf_list[seq_id] = []
        sys.stderr.write("SeqID: %s\n" % seq_id)
    
    if seq_id.startswith('chrUn_'):
        gxf_list['chrUn'].append(line.strip())
    else:
        gxf_list[seq_id].append(line.strip())
f_gxf.close()

for tmp_id, tmp_list in gxf_list.items():
    filename_out = '%s.%s.%s' % (filename_base, tmp_id, file_type)
    sys.stderr.write('Write %s\n' % filename_out)
    f_out = open(filename_out, 'w')
    if version_str != 'NA':
        f_out.write("%s\n" % version_str)
    f_out.write("%s\n" % "\n".join(tmp_list))
    f_out.close()
