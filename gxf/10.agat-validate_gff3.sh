#!/bin/bash

# install AGAT via bioconda first.

GFF_IN=$1
GFF_OUT=$GFF_IN".validated"
agat_convert_sp_gxf2gxf.pl -g $GFF_IN -o $GFF_OUT
