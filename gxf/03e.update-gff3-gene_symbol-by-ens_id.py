#!/usr/bin/env python3
import gzip
import sys

filename_gff = sys.argv[1]
filename_gene_info = sys.argv[2]

version_gene_info = filename_gene_info.replace('.gz', '').split('.')[-2]
filename_out = '%s.updated.%s' % (filename_gff, version_gene_info)

symbol_list = dict()
f_gene_info = open(filename_gene_info, 'r')
if filename_gene_info.endswith('.gz'):
    f_gene_info = gzip.open(filename_gene_info, 'rt')

# gene_info v2
# GENE_SYMBOL XB_GENE_ID XB_MODEL_ID NCBI_GENE_ID ENSEMBL_GENE_ID GENE_NAME
idx_ens_id = -1
h_tokens = []
for line in f_gene_info:
    if line.startswith('#'):
        if line.startswith('# gene_info'):
            gene_info_version = line.strip().split()[-1]
        if line.find('ENSEMBL_GENE_ID') >= 0:
            h_tokens = line.strip().lstrip('#').split()
            idx_ens_id = h_tokens.index('ENSEMBL_GENE_ID')
        continue

    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0].split()[0]
    if tokens[0] != tmp_symbol:
        sys.stderr.write("Symbol changes: %s -> %s\n" % (tokens[0], tmp_symbol))
    tmp_ens_id = tokens[idx_ens_id]
    symbol_list[tmp_ens_id] = tmp_symbol
f_gene_info.close()

f_gff = open(filename_gff, 'r')
if filename_gff.endswith('.gz'):
    f_gff = gzip.open(filename_gff, 'rt')

f_out = open(filename_out, 'w')
f_log = open("%s.log" % filename_out, 'w')

for line in f_gff:
    tmp_line = line.strip()
    if tmp_line.startswith('#'):
        f_out.write("%s\n" % tmp_line)
        continue

    tokens = tmp_line.split("\t")
    tmp_feature = tokens[2]
    str_except_tag = "\t".join(tokens[:8])

    tag_list = []
    tag_dict = dict()
    tmp_gene_id = 'NA'
    tmp_gene_name = 'NA'

    for tmp in tokens[8].split(';'):
        tmp = tmp.strip()
        if tmp == '':
            continue

        (tmp_k, tmp_v) = tmp.strip().split("=")
        # tmp_v = tmp_v.replace('"', '')

        tag_list.append(tmp_k)
        tag_dict[tmp_k] = tmp_v
        if tmp_k == 'gene_id':
            tmp_gene_id = tmp_v
        elif tmp_k == 'Name':
            tmp_gene_name = tmp_v

    is_changed = -1
    if tmp_gene_id in symbol_list:
        if symbol_list[tmp_gene_id] != tmp_gene_name and tmp_gene_name != 'NA':
            # sys.stderr.write("Update\t%s\t%s\t%s\t%s\n" %
            #                 (tmp_feature, tmp_gene_id, tmp_gene_name,
            #                  symbol_list[tmp_gene_id]))
            f_log.write("Update\t%s\t%s\t%s\t%s\n" %
                        (tmp_feature, tmp_gene_id, tmp_gene_name,
                         symbol_list[tmp_gene_id]))
            
            tag_dict['Name'] = symbol_list[tmp_gene_id]
            is_changed = 1
        elif tmp_gene_name == 'NA':
            if tmp_feature == 'gene' or tmp_feature == 'mRNA':
                f_log.write("Update\t%s\t%s\t%s\t%s\n" %
                            (tmp_feature, tmp_gene_id, tmp_gene_name,
                             symbol_list[tmp_gene_id]))

                tag_dict['Name'] = symbol_list[tmp_gene_id]
                tag_list.append('Name')
                is_changed = 1

    if is_changed > 0:
        new_tag = ";".join(['%s=%s' %
                             (tmp_k, tag_dict[tmp_k]) for tmp_k in tag_list])
        f_out.write("%s\t%s\n" % (str_except_tag, new_tag))
    else:
        f_out.write("%s\n" % tmp_line)

f_gff.close()
f_out.close()
