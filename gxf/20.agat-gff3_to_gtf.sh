#!/bin/bash

GFF_IN=$1
GTF_OUT=${GFF_IN/.gff3/}".gtf"
agat_convert_sp_gff2gtf.pl -o $GTF_OUT --gff $GFF_IN
