#!/usr/bin/env python3
import sys

# A script to make gene_info tables from the following script.
#
# https://xenbase-bio1.ucalgary.ca/cgi-bin/reports/gene_info_model.cgi

usage_mesg = 'Usage: report-gene_info-v2.py <temp file> <speceis_cide>'

if len(sys.argv) != 3:
    sys.stderr.write(" %s\n" % usage_mesg)
    sys.exit(1)

filename_tmp = sys.argv[1]
target_sp_id = sys.argv[2]

f_tmp = open(filename_tmp, 'r')
# print("%s\t%s\t%s\t%s\t%s\t%s" %
#       ("GENE_SYMBOL", "XB_GENE_ID", "XB_MODEL_ID",
#        "NCBI_GENE_ID", "ENSEMBL_GENE_ID", "GENE_NAME"))

for line in f_tmp:
    if line.startswith('#'):
        continue
    tokens = line.split("\t")
    gene_symbol = tokens[0]
    species_id = tokens[1]
    ncbi_gene_id = tokens[2]
    if ncbi_gene_id == '':
        ncbi_gene_id = 'N.A.'
    ens_gene_id = tokens[3]
    if ens_gene_id == '':
        ens_gene_id = 'N.A.'

    xb_gene_id = tokens[4]
    xb_genepage_id = tokens[5]

    xb_model_id = tokens[6]
    if xb_model_id == '':
        xb_model_id = 'N.A.'
    gene_name = tokens[7].strip()
    if gene_name == '':
        gene_name = 'N.A.'

    if gene_symbol == '':
        continue
    if xb_gene_id == '':
        continue

    if species_id == target_sp_id:
        print("%s\t%s\t%s\t%s\t%s\t%s" %
              (gene_symbol, xb_gene_id, xb_model_id,
               ncbi_gene_id, ens_gene_id, gene_name))
f_tmp.close()

# input
# GENE_SYMBOL,ORGANISM_ID,NCBI_GENE_ID,ENSEMBL_GENE_ID,
# XB_GENE_ID,XB_GENEPAGE_ID,MODEL_NAME,GENE_NAME

# output
# GENE_SYMBOL XB_GENE_ID XB_MODEL_ID NCBI_GENE_ID ENSEMBL_GENE_ID GENE_NAME
