# gene_info: a format to review GeneID and symbols

A gene_info file is a concise report for the Xenbase gene annotation information.
See [GitLab wiki page](https://gitlab.com/Xenbase/genomics/annot-tools/-/wikis/gene_info) for more information.

# Format
## 2024-08 version (v2)
The current version contains six columns as below (tab-delimited).
```
<gene symbol> <Xenbase GeneID> <Xenbase model ID> <NCBI GeneID> <EnsEMBL GeneID> <gene name>
```

It was changed to incorporate two more entities:
* EnsEMBL GeneID(i.e. ENSXETG00000021877): to integrate EnsEMBL annotation.
* Xenbase model ID(i.e. XBXL10_1g4446): to track the Xenbase genome annotation internally.

## Original version
The original version contains four columns as below (tab-delimited).
```
<gene symbol> <Xenbase GeneID> <NCBI GeneID> <gene name>
```

# How to make it
```
$ download-Xenbase-gene_info-v2.sh <target directory>
```
* [download-Xenbase-gene_info-v2.sh](https://gitlab.com/Xenbase/genomics/annot-tools/-/blob/main/gene_info/download-Xenbase-gene_info-v2.sh): create a gene_info table from the Xenbase production database.
  * It is based on Malcolm's CGI script https://xenbase-bio1.ucalgary.ca/cgi-bin/reports/gene_info_model.cgi
  * If you run this script, it will generate the following two files. 
    * XENLA_gene_info.Xenbase.<access date;YYYY-MM-DD>.tsv.gz
    * XENTR_gene_info.Xenbase.<access date;YYYY-MM-DD>.tsv.gz

# See also
* https://gitlab.com/Xenbase/genomics/xenLae10/-/tree/main/gene_info?ref_type=heads (gene_info for *X. laevis* v10 genome annotation)
* https://gitlab.com/Xenbase/genomics/xenTro10/-/tree/main/gene_info?ref_type=heads (gene_info for *X. tropicalis* v10 genome annotation)
