#!/usr/bin/env python3
import gzip
import sys

filename_gff = sys.argv[1]

f_gff = open(filename_gff, 'r')
if filename_gff.endswith('.gz'):
    f_gff = gzip.open(filename_gff, 'rt')

out_list = []
for line in f_gff:
    if line.startswith('#'):
        continue

    tokens = line.strip().split("\t")
    chr_id = tokens[0]
    tmp_mol_type = tokens[2]
    tmp_start = int(tokens[3])
    tmp_tag = tokens[8]

    if tmp_mol_type != 'gene':
        continue
    if tmp_tag.find('biotype=protein_coding') < 0:
        continue

    tmp_symbol = 'N.A.'
    tmp_name = 'N.A.'
    tmp_xb_id = 'N.A.'
    tmp_ncbi_id = 'N.A.'
    tmp_ens_id = 'N.A.'
    for tmp in tmp_tag.split(';'):
        if tmp.startswith('Dbxref='):
            for tmp2 in tmp.replace('Dbxref=', '').split(','):
                if tmp2.startswith('Xenbase:'):
                    tmp_xb_id = tmp2.split(':')[1]
                if tmp2.startswith('GeneID:'):
                    tmp_ncbi_id = tmp2
        if tmp.startswith('Name='):
            tmp_symbol = tmp.replace('Name=', '')
        if tmp.startswith('description='):
            tmp_name = tmp.replace('description=', '')

    out_list.append("%s\t%s\t%s\t%s\t%s" % (
                    tmp_symbol, tmp_xb_id, tmp_ncbi_id, tmp_ens_id, tmp_name))
f_gff.close()

print("#%s\t%s\t%s\t%s\t%s" %
      ("GENE_SYMBOL", "XB_GENE_ID", "NCBI_GENE_ID",
       "ENSEMBL_GENE_ID", "GENE_NAME"))
print("\n".join(sorted(out_list)))
