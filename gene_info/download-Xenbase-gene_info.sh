#!/bin/bash

DATE=$(date -I)

#OUT_DIR="/work/project/xenbase/gene_info/"
OUT_DIR=$1
OUT_TEMP=$OUT_DIR"gene_info."$DATE".tmp"

curl -o $OUT_TEMP https://xenbase-bio1.ucalgary.ca/cgi-bin/reports/Gene_info_table_v2.cgi

#HEADER=$(head -n 1 $OUT_TEMP | awk -F"\t" '{print $1"\t"$4"\t"$3"\t"$5}')
HEADER="#GENE_SYMBOL	XB_GENE_ID	NCBI_GENE_ID	ENSEMBL_GENE_ID	GENE_NAME"

OUT_XENLA=$OUT_DIR"XENLA_gene_info.Xenbase."$DATE".tsv"
echo $HEADER > $OUT_XENLA
awk -F"\t" '{if ($2==13) print $1"\t"$4"\t"$3"\tN.A.\t"$5 }' $OUT_TEMP | sort >> $OUT_XENLA
gzip $OUT_XENLA

OUT_XENTR=$OUT_DIR"XENTR_gene_info.Xenbase."$DATE".tsv"
echo $HEADER > $OUT_XENTR
awk -F"\t" '{if ($2==12) print $1"\t"$4"\t"$3"\tN.A.\t"$5 }' $OUT_TEMP | sort >> $OUT_XENTR
gzip $OUT_XENTR

rm $OUT_TEMP
