#!/usr/bin/env python
#
# check the "gene_id" in gene_info.tsv
#

import os
import re
import sys

filename_tsv = sys.argv[1]
filename_log = os.path.basename(filename_tsv)
filename_log = re.sub(r'.tsv', '', filename_log) + ".unique_id.log"

ncbi_gene_list = dict()
xb_gene_list = dict()

f_tsv = open(filename_tsv, 'r')
for line in f_tsv:
    if line.startswith('#'):
        continue

    tmp_line = line.strip()

    tokens = tmp_line.split("\t")
    tmp_symbol = tokens[0]
    tmp_ncbi_id = tokens[1]
    tmp_xb_id = tokens[2]
    tmp_name = tokens[3]

    if tmp_ncbi_id not in ncbi_gene_list:
        ncbi_gene_list[tmp_ncbi_id] = []
    ncbi_gene_list[tmp_ncbi_id].append(tmp_line)

    if tmp_xb_id not in xb_gene_list:
        xb_gene_list[tmp_xb_id] = []
    xb_gene_list[tmp_xb_id].append(tmp_line)
f_tsv.close()

f_log = open(filename_log, 'w')
for tmp_id, tmp_list in ncbi_gene_list.items():
    if len(tmp_list) == 1:
        continue
    else:
        sys.stderr.write("## Multiple NCBI GeneID: %s\n" % tmp_id)
        f_log.write("## Multiple Xenbase GeneID: %s\n" % tmp_id)
        for tmp_line in tmp_list:
            sys.stderr.write("%s\n" % tmp_line)
            f_log.write("%s\n" % tmp_line)
        sys.stderr.write('#\n')
        f_log.write("#\n")

for tmp_id, tmp_list in xb_gene_list.items():
    if len(tmp_list) == 1:
        continue
    else:
        sys.stderr.write("## Multiple Xenbase GeneID: %s\n" % tmp_id)
        f_log.write("## Multiple Xenbase GeneID: %s\n" % tmp_id)
        for tmp_line in tmp_list:
            sys.stderr.write("%s\n" % tmp_line)
            f_log.write("%s\n" % tmp_line)
        sys.stderr.write('#\n')
        f_log.write("#\n")
f_log.close()
