#!/usr/bin/env python
#
# check the "gene name" in gene_info.tsv
#

import re
import os
import sys

filename_tsv = sys.argv[1]
filename_log = os.path.basename(filename_tsv)
filename_log = re.sub(r'.tsv', '', filename_log) + ".unique_name.log"

name_list = dict()

f_tsv = open(filename_tsv, 'r')
for line in f_tsv:
    if line.startswith('#'):
        continue

    tmp_line = line.strip()

    tokens = tmp_line.split("\t")
    tmp_symbol = tokens[0]
    tmp_ncbi_id = tokens[1]
    tmp_xb_id = tokens[2]
    tmp_name = tokens[3]

    if tmp_ncbi_id not in name_list:
        name_list[tmp_ncbi_id] = {'name': [], 'line': []}
    name_list[tmp_ncbi_id]['name'].append(tmp_name)
    name_list[tmp_ncbi_id]['line'].append(tmp_line)
f_tsv.close()

count_multi = 0
sys.stderr.write("Write %s\n" % filename_log)

f_log = open(filename_log, 'w')
for tmp_id, tmp_list in name_list.items():
    if len(set(tmp_list['name'])) == 1:
        continue
    else:
        sys.stderr.write("## Multiple gene name: %s\n" % tmp_id)
        f_log.write("## Multiple gene name: %s\n" % tmp_id)
        count_multi += 1
        for tmp_line in tmp_list['line']:
            sys.stderr.write("%s\n" % tmp_line)
            f_log.write("%s\n" % tmp_line)
        sys.stderr.write('#\n')
        f_log.write("#\n")

sys.stderr.write("\nGenes with multiple names: %d\n\n" % count_multi)
