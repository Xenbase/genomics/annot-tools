#!/bin/bash

# Usage: download-Xenbase-gene_info-v2.sh <target_dir>

DATE=$(date -I)

OUT_DIR=$1
OUT_TEMP=$OUT_DIR"gene_info."$DATE".tmp"

if [ ! -e $OUT_TEMP ]; then
  echo "Download to $OUT_TEMP"
 
  # version 2
  curl -o $OUT_TEMP https://xenbase-bio1.ucalgary.ca/cgi-bin/reports/gene_info_model.cgi

  # version 1
  # curl -o $OUT_TEMP https://xenbase-bio1.ucalgary.ca/cgi-bin/reports/Gene_info_table_v2.cgi
fi

#HEADER=$(head -n 1 $OUT_TEMP | awk -F"\t" '{print $1"\t"$4"\t"$3"\t"$5}')

FILE_DATE=$(head -n 1 $OUT_TEMP)
VERSION="gene_info v2"
HEADER="GENE_SYMBOL	XB_GENE_ID	XB_MODEL_ID	NCBI_GENE_ID	ENSEMBL_GENE_ID	GENE_NAME"

OUT_XENLA=$OUT_DIR"XENLA_gene_info.Xenbase."$DATE".tsv"
echo "# "$VERSION > $OUT_XENLA
# echo "# "${FILE_DATE/^# //} >> $OUT_XENLA
echo "# "$HEADER >> $OUT_XENLA
$(dirname $0)/report-gene_info-v2.py $OUT_TEMP 13 | sort >> $OUT_XENLA

gzip $OUT_XENLA

OUT_XENTR=$OUT_DIR"XENTR_gene_info.Xenbase."$DATE".tsv"
echo "# "$VERSION > $OUT_XENTR
# echo "# "$FILE_DATE >> $OUT_XENTR
echo "# "$HEADER >> $OUT_XENTR

$(dirname $0)/report-gene_info-v2.py $OUT_TEMP 12 | sort >> $OUT_XENTR
gzip $OUT_XENTR

# rm $OUT_TEMP
