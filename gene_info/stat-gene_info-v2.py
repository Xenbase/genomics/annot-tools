#!/usr/bin/env python3
import sys

filename_gene_info = sys.argv[1]

filename_out = "%s.stat.log" % filename_gene_info
f_out = open(filename_out, 'w')
f_out.write("#Summary Input gene_info: %s\n\n" % filename_gene_info)

f_gene_info = open(filename_gene_info, 'r')
if filename_gene_info.endswith('.gz'):
    import gzip
    f_gene_info = gzip.open(filename_gene_info, 'rt')

count_line = 0
symbol_list = dict()
xb_gene_id_list = dict()
ncbi_gene_id_list = dict()
ens_gene_id_list = dict()

count_provisional = 0
# gene_info v2
# GENE_SYMBOL XB_GENE_ID XB_MODEL_ID NCBI_GENE_ID ENSEMBL_GENE_ID GENE_NAME
for line in f_gene_info:
    if line.startswith('#'):
        continue
    tmp_line = line.strip()
    tokens = tmp_line.split("\t")
    count_line += 1

    tmp_gene_symbol = tokens[0]
    tmp_xb_gene_id = tokens[1]
    tmp_xb_model_id = tokens[2]
    tmp_ncbi_gene_id = tokens[3]
    tmp_ens_gene_id = tokens[4]
    tmp_gene_name = tokens[5]

    if tmp_gene_symbol.find('provisional') >= 0:
        count_provisional += 1
        f_out.write("##Provisional\t%s\n" % tmp_line)

    if tmp_gene_symbol not in symbol_list:
        symbol_list[tmp_gene_symbol] = []
    symbol_list[tmp_gene_symbol].append(tmp_line)

    if tmp_xb_gene_id not in xb_gene_id_list:
        xb_gene_id_list[tmp_xb_gene_id] =[]
    xb_gene_id_list[tmp_xb_gene_id].append(tmp_line)
    
    if tmp_ncbi_gene_id not in ncbi_gene_id_list:
        ncbi_gene_id_list[tmp_ncbi_gene_id] =[]
    ncbi_gene_id_list[tmp_ncbi_gene_id].append(tmp_line)
    
    if tmp_ens_gene_id not in ens_gene_id_list:
        ens_gene_id_list[tmp_ens_gene_id] =[]
    ens_gene_id_list[tmp_ens_gene_id].append(tmp_line)

f_gene_info.close()

print("Total records: %d" % count_line)
f_out.write("\n#Summary Total provisional gene symbols: %d\n" % count_provisional)
f_out.write("\n#Summary Total records: %d\n" % count_line)

count_multi_symbol = 0
for tmp_gene_symbol, tmp_list in symbol_list.items():
    if len(tmp_list) > 1:
        count_multi_symbol += 1
        for tmp_line in tmp_list:
            f_out.write("#MultiSymbol\t%s\n" % tmp_line)

print("Gene symbol with multiple records: %d" % count_multi_symbol)
f_out.write("\n#Summary Gene symbol with multiple records: %d\n" % count_multi_symbol)

count_multi_xb_gene_id = 0
for tmp_xb_gene_id, tmp_list in xb_gene_id_list.items():
    if len(tmp_list) > 1:
        count_multi_xb_gene_id += 1
        for tmp_line in tmp_list:
            f_out.write("#MultiID-XB\t%s\n" % tmp_line)

print("XB-GENE-IDs with multiple records: %d" % count_multi_xb_gene_id)
f_out.write("\n#Summary XB-GENE-IDs with multiple records: %d\n" % count_multi_xb_gene_id)

count_multi_ncbi_gene_id = 0
for tmp_ncbi_gene_id, tmp_list in ncbi_gene_id_list.items():
    if tmp_ncbi_gene_id == 'N.A.':
        continue
    if len(tmp_list) > 1:
        count_multi_ncbi_gene_id += 1
        for tmp_line in tmp_list:
            f_out.write("#MultiID-NCBI\t%s\n" % tmp_line)

print("NCBI-GeneIDs with multiple records: %d" % count_multi_ncbi_gene_id)
f_out.write("\n#Summary NCBI-GeneIDs with multiple records: %d\n" % count_multi_ncbi_gene_id)

count_multi_ens_gene_id = 0
for tmp_ens_gene_id, tmp_list in ens_gene_id_list.items():
    if tmp_ens_gene_id == 'N.A.':
        continue
    if len(tmp_list) > 1:
        count_multi_ens_gene_id += 1
        for tmp_line in tmp_list:
            f_out.write("#MultiID-EnsEMBL\t%s\n" % tmp_line)

print("EnsEMBL-GeneIDs with multiple records: %d" % count_multi_ens_gene_id)
f_out.write("\n#Summary EnsEMBL-GeneIDs with multiple records: %d\n" % count_multi_ens_gene_id)

