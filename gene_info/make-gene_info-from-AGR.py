#!/usr/bin/env python3
import sys

# Input: the tsv file from SimpleMine
#         https://www.alliancegenome.org/agr_simplemine.cgi
#
# 1. Select species ("Xenopus laevis" or "Xenopus tropicalis").
# 2. Choose "case sensitive input".
# 3. Choose " download results as a tab-delimited file".
# 4. Choose "keep duplicate gene entries in results".
# 5. In "Step 3: Choose types of information to retrieve",
#    choose the following entities:
#    - Gene ID
#    - Gene Symbol
#    - Gene Name
#    - NCBI ID
#    - ENSEMBL ID
# 6. Push the button "Query all genes in this species".
#

filename_raw = sys.argv[1]

f_raw = open(filename_raw, 'r')
out_list = []
for line in f_raw:
    if line.startswith('#') or line.startswith('Your'):
        continue
    tokens = line.strip().split("\t")
    tmp_xb_id = tokens[0].split(':')[1]
    tmp_symbol = tokens[2].strip()
    tmp_name = tokens[3]
    if tokens[4] != 'N.A.':
        tmp_ncbi_id = "GeneID:%s" % tokens[4]
    else:
        tmp_ncbi_id = "N.A."
    tmp_ens_id = tokens[5]

    out_list.append("%s\t%s\t%s\t%s\t%s" %
                    (tmp_symbol, tmp_xb_id, tmp_ncbi_id, tmp_ens_id, tmp_name))
f_raw.close()

print("#%s\t%s\t%s\t%s\t%s" %
      ("GENE_SYMBOL", "XB_GENE_ID", "NCBI_GENE_ID",
       "ENSEMBL_GENE_ID", "GENE_NAME"))
print("\n".join(sorted(out_list)))
