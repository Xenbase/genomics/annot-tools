This [annot-tool](https://gitlab.com/Xenbase/genomics/annot-tools/) project 
contains a collection of scripts used by the Xenbase team 
for Xenopus genome annotation. 

It is maintained by [Taejoon Kwon](https://gitlab.com/taejoon/) at UNIST, Korea. 

See [the project wiki](https://gitlab.com/Xenbase/genomics/annot-tools/-/wikis/home) for more information. 

## Directories
* gene_info - a format to review the Gene IDs and gene symbols.
  * See [wiki page](https://gitlab.com/Xenbase/genomics/annot-tools/-/wikis/gene_info) for more details.
  * Version 1 (2023.12. ~)
    * http://xenbase-pub.amphibase.org/annotation/gene_info/
  * Version 2, with gene_model and EnsEMBL ID (2024.08. ~)
    * http://xenbase-pub.amphibase.org/annotation/gene_info.v2/
* gxf - a format for genome annotation (GFF3 & GTF)
  * [[Concise_GFF3]]
* genome - about genome itself.
* orthology - about orthology/paralogy. 
* uniprot - about data from the UniProt.
* xenbase - about data from the Xenbase.
