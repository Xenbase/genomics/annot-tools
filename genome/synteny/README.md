# annot-tools/genome/synteny

This is the collection of scripts for synteny analaysis by the Xenbase team.

## A pairwise alignment for Jbrowse2

It is quite straightforward to prepare the pairwise alignment for JBrowse2 because it only requires a PAF file (https://github.com/lh3/miniasm/blob/master/PAF.md) between two genomes. 

See [PairwiseSynteny](https://gitlab.com/Xenbase/genomics/annot-tools/-/wikis/PairwiseSynteny) wiki page for more information. 
