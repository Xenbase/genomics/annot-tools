#!/bin/bash

# Use a hard-masked genome for both query and target.
# Remove scaffolds and chrM before running.

# xenLae10.chrS -> xenTro10.chr
TARGET="xenTro10_rm.XB-2024-02.chr.fa"
QUERY="xenLae10_rm.XB-2024-02.chrS.fa"
OUT_PAF="xenLae10_rm_chrS.xenTro10_rm_chr.paf"

echo "**** Make $OUT_PAF ****"
echo
minimap2 -x asm20 -c $TARGET $QUERY -o $OUT_PAF

# xenLae10.chrL -> xenTro10.chr
QUERY="xenLae10_rm.XB-2024-02.chrL.fa"
OUT_PAF="xenLae10_rm_chrL.xenTro10_rm_chr.paf"

echo "**** Make $OUT_PAF ****"
echo
minimap2 -x asm20 -c $TARGET $QUERY -o $OUT_PAF

# xenLae10.chrL -> xenTro10.chr

QUERY="xenLae10_rm.XB-2024-02.chrS.fa"
TARGET="xenLae10_rm.XB-2024-02.chrL.fa"
OUT_PAF="xenLae10_rm_chrS.xenLae10_rm_chrL.paf"

echo "**** Make $OUT_PAF ****"
echo
minimap2 -x asm20 -c $TARGET $QUERY -o $OUT_PAF
