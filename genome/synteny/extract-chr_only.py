#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]
filename_base = filename_fa.replace('.fa', '')

f_fa = open(filename_fa, 'r')
if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')
    filename_base = filename_fa.replace('.fa.gz', '')

filename_chr = '%s.chr.fa' % filename_base

sys.stderr.write("%s -> %s\n" % (filename_fa, filename_chr))
f_chr = open(filename_chr, 'w')

which_chr = 'NA'
for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>').split()[0]
        if tmp_h.startswith('chr'):
            if tmp_h.startswith('chrUn'):
                which_chr = 'NA'
            elif tmp_h.startswith('chrM'):
                which_chr = 'NA'
            else:
                which_chr = 'chr'
                f_chr.write('%s\n' % line.strip())
        else:
            which_chr = 'NA'
    elif which_chr == 'chr':
        f_chr.write("%s\n" % line.strip())
f_fa.close()
