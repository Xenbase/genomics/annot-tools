#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]
filename_base = filename_fa.replace('.fa', '')

f_fa = open(filename_fa, 'r')
if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')
    filename_base = filename_fa.replace('.fa.gz', '')

filename_chrL = "%s.chrL.fa" % filename_base
filename_chrS = "%s.chrS.fa" % filename_base

sys.stderr.write("%s -> %s, %s\n" %
                 (filename_fa, filename_chrL, filename_chrS))

f_chrL = open(filename_chrL, 'w')
f_chrS = open(filename_chrS, 'w')

which_chr = 'NA'
for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>').split()[0]
        if tmp_h.startswith('chr'):
            if tmp_h.startswith('chrUn'):
                which_chr = 'NA'
            elif tmp_h.startswith('chrM'):
                which_chr = 'NA'
            elif tmp_h.endswith('L'):
                which_chr = 'chrL'
                f_chrL.write('%s\n' % line.strip())
            elif tmp_h.endswith('S'):
                which_chr = 'chrS'
                f_chrS.write('%s\n' % line.strip())
            else:
                which_chr = 'NA'
        else:
            which_chr = 'NA'
    elif which_chr == 'chrL':
        f_chrL.write("%s\n" % line.strip())
    elif which_chr == 'chrS':
        f_chrS.write("%s\n" % line.strip())
f_fa.close()
