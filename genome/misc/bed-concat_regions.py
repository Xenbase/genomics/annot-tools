#!/usr/bin/env python3
import sys

filename_bed = sys.argv[1]

len_list = dict()

f_bed = open(filename_bed, 'r')

if filename_bed.endswith('.gz'):
    import gzip
    f_bed = gzip.open(filename_bed, 'rt')

for line in f_bed:
    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    if seq_id not in len_list:
        len_list[seq_id] = 0
    len_list[seq_id] += int(tokens[2]) - int(tokens[1])
f_bed.close()

total_len = 0
for tmp_id in sorted(len_list.keys()):
    total_len += len_list[tmp_id]
    print("%s\t%d" % (tmp_id, len_list[tmp_id]))

print("Total length: %d" % total_len)
