#!/usr/bin/env python3
import sys

filename_in = sys.argv[1]
filename_out = '%s.StartZero' % filename_in

sys.stderr.write("%s -> %s\n" % (filename_in, filename_out))

f_in = open(filename_in, 'r')
if filename_in.endswith('.gz'):
    import gzip
    f_in = gzip.open(filename_in, 'rt')

f_out = open(filename_out, 'w')
for line in f_in:
    tokens = line.strip().split("\t")
    new_start = int(tokens[1]) - 1
    new_end = int(tokens[2]) - 1
    f_out.write("%s\t%d\t%d\t%s\n" %
                (tokens[0], new_start, new_end,
                 "\t".join(tokens[3:])))
f_in.close()
f_out.close()
