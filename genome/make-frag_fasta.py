#!/usr/bin/env python3
import sys

filename_masked_fa = sys.argv[1]

frag_len = 500
frag_step = 100

seq_list = dict()
f_fa = open(filename_masked_fa, 'r')
if filename_masked_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_masked_fa, 'rt')

for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        seq_list[tmp_h] = []
    else:
        seq_list[tmp_h].append(line.strip())
f_fa.close()

for tmp_h, tmp_list in seq_list.items():
    if tmp_h.startswith('chrUn') or tmp_h.startswith('chrM'):
        continue

    tmp_seq = ''.join(tmp_list)
    for i in range(0, len(tmp_seq), frag_step):
        tmp_frag = tmp_seq[i:i+frag_len]

        # skip the repeats
        if tmp_frag.count('N') > frag_len * 0.5:
            continue

        # skip the 3'-ends
        if len(tmp_frag) < frag_len:
            continue

        print(">%s_%d_%d\n%s" % (tmp_h, i, i+frag_len, tmp_frag))
