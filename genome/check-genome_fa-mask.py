#!/usr/bin/env python3
import sys


filename_fa = sys.argv[1]

f_fa = open(filename_fa, 'r')

if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')

sys.stderr.write('Read %s ...\n' % filename_fa)

chr_list = dict()
for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>').split()[0]
        chr_list[tmp_h] = {'total': 0, 'hard': 0, 'soft': 0}

        sys.stderr.write('Read %s\n' % tmp_h)
    else:
        tmp_line = line.strip()
        tmp_total = len(tmp_line)

        tmp_hard = tmp_line.count('N') + tmp_line.count('n')
        tmp_soft = tmp_line.count('a') + tmp_line.count('t')
        tmp_soft += tmp_line.count('g') + tmp_line.count('c')
        tmp_soft += tmp_line.count('n')

        chr_list[tmp_h]['total'] += tmp_total
        chr_list[tmp_h]['hard'] += tmp_hard
        chr_list[tmp_h]['soft'] += tmp_soft
f_fa.close()

sum_total = 0
sum_hard = 0
sum_soft = 0

for tmp_h, tmp in sorted(chr_list.items()):
    sum_total += tmp['total']
    sum_hard += tmp['hard']
    sum_soft += tmp['soft']
    ratio_hard = tmp['hard'] * 100.0 / tmp['total']
    ratio_soft = tmp['soft'] * 100.0 / tmp['total']

    sys.stdout.write("%s: length=%s, " % (tmp_h, '{:,}'.format(tmp['total'])))
    sys.stdout.write("soft_masked=%s (%.2f pct), " %
                     ('{:,}'.format(tmp['soft']), ratio_soft))
    sys.stdout.write("hard_masked=%s (%.2f pct)\n " %
                     ('{:,}'.format(tmp['hard']), ratio_hard))


ratio_hard_sum = sum_hard * 100.0 / sum_total
ratio_soft_sum = sum_soft * 100.0 / sum_total

sys.stdout.write("\nTotal: length=%s, " % ('{:,}'.format(sum_total)))
sys.stdout.write("soft_masked=%s (%.2f pct), " %
                 ('{:,}'.format(sum_soft), ratio_soft_sum))
sys.stdout.write("hard_masked=%s (%.2f pct)\n " %
                 ('{:,}'.format(sum_hard), ratio_hard_sum))
