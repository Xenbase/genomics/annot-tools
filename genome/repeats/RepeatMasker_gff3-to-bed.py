#!/usr/bin/env python3
import sys

filename_RM_out = sys.argv[1]

f_RM_out = open(filename_RM_out, 'r')
if filename_RM_out.endswith('.gz'):
    import gzip
    f_RM_out = gzip.open(filename_RM_out, 'rt')

repeat_id = 1
for tmp_line in f_RM_out:
    tmp_line = tmp_line.strip()
    if tmp_line.startswith('#'):
        continue

    tokens = tmp_line.strip().split("\t")

    chr_name = tokens[0]
    chr_start = int(tokens[3])
    chr_end = int(tokens[4])

    tmp_tokens = tokens[8].replace('Name ', '').replace(';class', '').split()

    repeat_motif = tmp_tokens[0]
    repeat_class = tmp_tokens[3]

    # needs to make a position to starting with zero
    repeat_start = int(tmp_tokens[1]) - 1
    repeat_end = int(tmp_tokens[2]) - 1
    repeat_left = -1
    repeat_id += 1

    tmp_name = 'ID=%s;Name=%s;Class=%s;begin=%d;end=%d;left=%d' %\
               (repeat_id, repeat_motif, repeat_class,
                repeat_start, repeat_end, repeat_left)
    print("%s\t%d\t%d\t%s" % (chr_name, chr_start, chr_end, tmp_name))
f_RM_out.close()
