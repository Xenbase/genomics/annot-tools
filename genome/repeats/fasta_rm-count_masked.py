#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]

seqlen_list = dict()

f_fa = open(filename_fa, 'r')
if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')

for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        seqlen_list[tmp_h] = {'total': 0, 'masked': 0}
    else:
        seqlen_list[tmp_h]['total'] += len(line.strip())
        seqlen_list[tmp_h]['masked'] += line.strip().count('N')
f_fa.close()

total_len = 0
total_masked_len = 0
for tmp_id in sorted(seqlen_list.keys()):
    total_len += seqlen_list[tmp_id]['total']
    total_masked_len += seqlen_list[tmp_id]['masked']
    print("%s\t%d\t%d" % (tmp_id, seqlen_list[tmp_id]['masked'],
                          seqlen_list[tmp_id]['total']))

print("Total length: %d" % total_len)
print("Total masked length: %d" % total_masked_len)
