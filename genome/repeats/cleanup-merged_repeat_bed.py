#!/usr/bin/env python3

filename_in = 'xenLae10_merged.repeat.merge_d10.bed'
filename_out = 'xenLae10_repeats.XB-2024-01.bed'

# filename_in = 'xenTro10_merged.repeat.merge_d10.bed'
# filename_out = 'xenTro10_repeats.XB-2024-01.bed'

rep_id = 1
f_in = open(filename_in, 'r')
f_out = open(filename_out, 'w')
for line in f_in:
    tokens = line.strip().split("\t")
    bed_base = "\t".join(tokens[:3])

    name_list = dict()
    source_list = dict()
    class_list = dict()

    for tmp in tokens[3].split(','):
        tmp_source = 'N.A.'
        tmp_name = 'N.A.'
        tmp_class = 'N.A.'
        for tmp2 in tmp.split(';'):
            if tmp2.startswith('ID='):
                tmp_source = tmp2.split('.')[0].replace('ID=', '')
                if tmp_source not in source_list:
                    source_list[tmp_source] = 0
                source_list[tmp_source] += 1
            if tmp2.startswith('Class='):
                tmp_class = tmp2.split('=')[1].replace('"', '')
                if tmp_class not in class_list:
                    class_list[tmp_class] = 0
            if tmp2.startswith('Name='):
                tmp_name = tmp2.split('=')[1].replace('"', '')
                tmp_name = tmp_name.replace('Motif:', '')
                if tmp_name not in name_list:
                    name_list[tmp_name] = 0
                name_list[tmp_name] += 1

    sorted_name_list = sorted(name_list.keys(), key=name_list.get)
    rep_name = sorted_name_list[-1]

    alt_names = 'N.A.'
    if len(sorted_name_list) > 1:
        alt_names = ",".join(sorted_name_list[:-1])

    rep_class = 'N.A.'
    sorted_class_list = sorted([x for x in class_list.keys() if x != 'N.A.'],
                               key=class_list.get)
    if len(sorted_class_list) > 0:
        rep_class = sorted([x for x in class_list.keys() if x != 'N.A.'],
                           key=class_list.get)[-1]

    source_str = ",".join(sorted(source_list.keys()))
    f_out.write("%s\tID=XB-REP-%d;Name=%s;Class=%s;Source=%s;AltNames=%s\n" %
                (bed_base, rep_id, rep_name, rep_class, source_str, alt_names))
    rep_id += 1
f_in.close()
f_out.close()
