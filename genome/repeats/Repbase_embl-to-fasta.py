#!/usr/bin/env python3
import sys
import gzip

filename_embl = sys.argv[1]

f_embl = open(filename_embl, 'r')
if filename_embl.endswith('.gz'):
    f_embl = gzip.open(filename_embl, 'rt')

cur_id = 'NA'
cur_tag = 'NA'

rfam_list = dict()
for line in f_embl:
    tmp_tag = line[:2]
    if tmp_tag == 'CC':
        continue

    if tmp_tag != '  ':
        cur_tag = tmp_tag

    if cur_tag == 'ID':
        cur_id = line[5:].strip().split()[0]
        rfam_list[cur_id] = {'SQ': []}
    elif cur_tag == '//':
        cur_id = 'NA'
    elif cur_tag == 'SQ':
        tmp_line = " ".join(line[5:].strip().split()[:-1])
        rfam_list[cur_id]['SQ'].append(tmp_line)
f_embl.close()

for tmp_id, tmp_rfam in rfam_list.items():
    print(">%s" % tmp_id)
    print("%s" % "\n".join(tmp_rfam['SQ'][1:]))
