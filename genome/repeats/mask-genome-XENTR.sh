#!/bin/bash

REP_BED="xenTro10_repeats.XB-2024-02.bed"

# Make sure that the input FASTA file is not masked.
# FA_IN="$HOME/project.xenopus/db/xenTro10.fa"
FA_IN="$HOME/project.xenopus/db/xenTro10.fa.unmasked"

FA_SM="xenTro10_sm.XB-2024-02.fa"
FA_RM="xenTro10_rm.XB-2024-02.fa"

echo $FA_SM
# bedtools maskfasta -fi $FA_IN -soft -fo $FA_SM -bed $REP_BED -fullHeader

echo $FA_RM
bedtools maskfasta -fi $FA_IN -fo $FA_RM -bed $REP_BED -fullHeader
