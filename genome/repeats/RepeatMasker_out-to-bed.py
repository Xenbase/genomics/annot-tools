#!/usr/bin/env python3
import sys

filename_RM_out = sys.argv[1]

f_RM_out = open(filename_RM_out, 'r')
if filename_RM_out.endswith('.gz'):
    import gzip
    f_RM_out = gzip.open(filename_RM_out, 'rt')

for tmp_line in f_RM_out:
    tmp_line = tmp_line.strip()
    if tmp_line.startswith("SW") or tmp_line.startswith("score"):
        continue

    tokens = tmp_line.strip().split()

    if len(tokens) < 15:
        continue

    chr_name = tokens[4]
    chr_start = int(tokens[5])
    chr_end = int(tokens[6])
    repeat_motif = tokens[9]
    repeat_class = tokens[10]

    # needs to make a position to starting with zero
    repeat_start = int(tokens[11].replace('(', '').replace(')', '')) - 1
    repeat_end = int(tokens[12].replace('(', '').replace(')', '')) - 1
    repeat_left = int(tokens[13].replace('(', '').replace(')', '')) - 1
    repeat_id = tokens[14]

    tmp_name = 'ID=%s;Name=%s;Class=%s;begin=%d;end=%d;left=%d' %\
               (repeat_id, repeat_motif, repeat_class,
                repeat_start, repeat_end, repeat_left)
    print("%s\t%d\t%d\t%s" % (chr_name, chr_start, chr_end, tmp_name))
f_RM_out.close()
