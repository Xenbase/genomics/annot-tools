#!/usr/bin/env python3
import sys
import gzip

filename_embl = sys.argv[1]

f_embl = open(filename_embl, 'r')
if filename_embl.endswith('.gz'):
    f_embl = gzip.open(filename_embl, 'rt')

cur_id = 'NA'
cur_tag = 'NA'

dfam_list = dict()
for line in f_embl:
    tmp_tag = line[:2]
    if tmp_tag == 'CC':
        continue

    if tmp_tag != '  ':
        cur_tag = tmp_tag

    if cur_tag == 'ID':
        cur_id = line[5:].strip().split(';')[0]
        dfam_list[cur_id] = {'NM': [], 'DR': [], 'KW': [], 'SQ': []}
    elif cur_tag == '//':
        cur_id = 'NA'
    elif cur_tag == 'NM':
        dfam_list[cur_id]['NM'].append(line[5:].strip())
    elif cur_tag == 'DR':
        dfam_list[cur_id]['DR'].append(line[5:].strip())
    elif cur_tag == 'KW':
        dfam_list[cur_id]['KW'].append(line[5:].strip())
    elif cur_tag == 'SQ':
        tmp_line = " ".join(line[5:].strip().split()[:-1])
        dfam_list[cur_id]['SQ'].append(tmp_line)
f_embl.close()


for tmp_id, tmp_dfam in dfam_list.items():
    tmp_h = tmp_dfam['NM']
    tmp_name = ';'.join(tmp_dfam['NM'])
    tmp_DR = ';'.join(tmp_dfam['DR'])
    tmp_KW = ';'.join(tmp_dfam['KW'])
    print(">%s|%s DR=%s KW=%s" % (tmp_id, tmp_name, tmp_DR, tmp_KW))
    print("%s" % "\n".join(tmp_dfam['SQ'][1:]))
