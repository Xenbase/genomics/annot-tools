#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]
filename_out = "%s.unmasked" % filename_fa

f_fa = open(filename_fa, 'r')
if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')

sys.stderr.write("Write %s ... " % filename_out)

f_out = open(filename_out, 'w')
for line in f_fa:
    if line.startswith('>'):
        f_out.write("%s\n" % line.strip())
    else:
        f_out.write("%s\n" % line.strip().upper())
f_out.close()
f_fa.close()

sys.stderr.write("Done\n")
