#!/bin/bash

# sort raw bed with "sort -k1,1V -k2,2n <input bed>" first

bedtools merge -i xenLae10_merged.repeat.raw.bed -d 10 -c 4 -o collapse > xenLae10_merged.repeat.merge_d10.bed

# bedtools merge -i xenTro10_merged.repeat.raw.bed -d 10 -c 4 -o collapse > xenTro10_merged.repeat.merge_d10.bed
