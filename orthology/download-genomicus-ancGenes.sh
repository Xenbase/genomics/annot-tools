#!/bin/bash

VERSION="110.01"
EXT="tar" # .tgz or .zip

SRC_NAME="ancGenes."$EXT
OUT_NAME="ancGenes."$VERSION"."$EXT
curl -o $OUT_NAME https://ftp.bio.ens.psl.eu/pub/dyogen/genomicus/$VERSION/$SRC_NAME
