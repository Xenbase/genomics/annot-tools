#!/bin/bash
URL_BASE="https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/UP000186698/"
FILE_LIST=$(dirname $0)"/FILE_LIST.uniprot_XENTR"

DATE=$(date -I)
# OUT_DIR="$HOME/project.xenopus/xenopus.annot/uniprot/XENTR/"$DATE

BASE_DIR=$1
OUT_DIR="$BASE_DIR/XENTR/"$DATE
echo $OUT_DIR

if [ ! -e $OUT_DIR ]; then
  echo "Make $OUT_DIR directory."
  mkdir $OUT_DIR
fi

for FILE in $(cat $FILE_LIST)
do
  echo $FILE
  URL_FILE=$URL_BASE"/"$FILE
  curl --output-dir $OUT_DIR -O $URL_FILE
done
