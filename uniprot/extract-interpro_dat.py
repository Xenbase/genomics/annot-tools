#!/usr/bin/env python3
import sys
import gzip

filename_csv = '../XENLA_prot_id_list.active.csv'
# UNIPROT_ACC,NCBI_PROT,NCBI_TX,NCBI_GENE,XB_GENE
# A0A060A0J8,XP_018107197.1,XM_018251708.2,GeneID:443604,XB-GENE-6252020

# path for protein2ipr.dat.gz
filename_ipr = sys.argv[1]
# A0A060A0J8      IPR000974       Glycoside hydrolase, family 22, lysozyme        PR00137 21      39

up_list = dict()
f_csv = open(filename_csv, 'r')
for line in f_csv:
    up_id = line.strip().split(",")[0]
    if up_id == 'NA':
        continue
    up_list[up_id] = 1
f_csv.close()


f_ipr = gzip.open(filename_ipr, 'rt')
for line in f_ipr:
    tmp_prot_id = line.strip().split("\t")[0]
    if tmp_prot_id in up_list:
        print(line.strip())
f_ipr.close()
